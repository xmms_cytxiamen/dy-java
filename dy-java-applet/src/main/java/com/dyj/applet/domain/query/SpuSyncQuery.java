package com.dyj.applet.domain.query;

import com.alibaba.fastjson.JSONObject;
import com.dyj.applet.domain.SpuEntryInfo;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-29 14:50
 **/
public class SpuSyncQuery extends BaseQuery {

    /**
     * 入口信息
     */
    private SpuEntryInfo entry_info;

    /**
     * 前台品类标签
     */
    private List<String> front_category_tag;

    /**
     * 推荐语，5~20个字
     */
    private String recommend_word;

    /**
     * 排序权重，按降序排列
     */
    private Long sort_weight;

    /**
     * 接入方店铺ID列表
     */
    private List<String> supplier_ext_id_list;

    /**
     * 价格，单位分
     */
    private Long price;

    /**
     * 接入方商品ID
     */
    private String spu_ext_id;

    /**
     * SPU图片，预售券必传
     */
    private List<String> image_list;

    /**
     * 小程序结算类型 1-包销 2-代销
     */
    private Integer mp_settle_type;

    /**
     * 价格，单位分
     */
    private Long origin_price;

    /**
     * 软件服务费率，万分数
     */
    private Long take_rate;

    /**
     * SPU属性字段
     */
    private JSONObject attribute;

    /**
     * 商品亮点标签
     */
    private List<String> highlights;

    /**
     * 商品名
     */
    private String name;

    /**
     * 下单是否依赖日期
     */
    private Boolean order_depends_on_date;

    /**
     * spu类型号，1-日历房，30-酒店民宿预售券，90-门票，91-团购券
     */
    private Integer spu_type;
    /**
     * 	SPU状态， 1 - 在线; 2 - 下线
     */
    private Integer status;
    /**
     * 库存
     */
    private Long stock;

    public static SpuSyncQueryBuilder builder() {
        return new SpuSyncQueryBuilder();
    }

    public static class SpuSyncQueryBuilder {
        private SpuEntryInfo entryInfo;
        private Long price;
        private Long originPrice;
        private Long takeRate;
        private Long stock;
        private String spuExtId;
        private List<String> highlights;
        private String name;
        private List<String> imageList;
        private Integer mpSettleType;
        private Integer spuType;
        private Integer status;
        private Long sortWeight;
        private List<String> supplierExtIdList;
        private String recommendWord;
        private Boolean orderDependsOnDate;
        private List<String> frontCategoryTag;
        private JSONObject attribute;
        private Integer tenantId;
        private String clientKey;

        public SpuSyncQueryBuilder entryInfo(SpuEntryInfo entryInfo) {
            this.entryInfo = entryInfo;
            return this;
        }
        public SpuSyncQueryBuilder price(Long price) {
            this.price = price;
            return this;
        }
        public SpuSyncQueryBuilder originPrice(Long originPrice) {
            this.originPrice = originPrice;
            return this;
        }
        public SpuSyncQueryBuilder takeRate(Long takeRate) {
            this.takeRate = takeRate;
            return this;
        }
        public SpuSyncQueryBuilder stock(Long stock) {
            this.stock = stock;
            return this;
        }
        public SpuSyncQueryBuilder spuExtId(String spuExtId) {
            this.spuExtId = spuExtId;
            return this;
        }
        public SpuSyncQueryBuilder highlights(List<String> highlights) {
            this.highlights = highlights;
            return this;
        }
        public SpuSyncQueryBuilder name(String name) {
            this.name = name;
            return this;
        }
        public SpuSyncQueryBuilder imageList(List<String> imageList) {
            this.imageList = imageList;
            return this;
        }
        public SpuSyncQueryBuilder mpSettleType(Integer mpSettleType) {
            this.mpSettleType = mpSettleType;
            return this;
        }
        public SpuSyncQueryBuilder spuType(Integer spuType) {
            this.spuType = spuType;
            return this;
        }
        public SpuSyncQueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }
        public SpuSyncQueryBuilder sortWeight(Long sortWeight) {
            this.sortWeight = sortWeight;
            return this;
        }
        public SpuSyncQueryBuilder supplierExtIdList(List<String> supplierExtIdList) {
            this.supplierExtIdList = supplierExtIdList;
            return this;
        }
        public SpuSyncQueryBuilder recommendWord(String recommendWord) {
            this.recommendWord = recommendWord;
            return this;
        }
        public SpuSyncQueryBuilder orderDependsOnDate(Boolean orderDependsOnDate) {
            this.orderDependsOnDate = orderDependsOnDate;
            return this;
        }
        public SpuSyncQueryBuilder frontCategoryTag(List<String> frontCategoryTag) {
            this.frontCategoryTag = frontCategoryTag;
            return this;
        }
        public SpuSyncQueryBuilder attribute(JSONObject attribute) {
            this.attribute = attribute;
            return this;
        }
        public SpuSyncQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public SpuSyncQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SpuSyncQuery build() {
            SpuSyncQuery spuSyncQuery = new SpuSyncQuery();
            spuSyncQuery.setEntry_info(entryInfo);
            spuSyncQuery.setPrice(price);
            spuSyncQuery.setOrigin_price(originPrice);
            spuSyncQuery.setTake_rate(takeRate);
            spuSyncQuery.setStock(stock);
            spuSyncQuery.setSpu_ext_id(spuExtId);
            spuSyncQuery.setHighlights(highlights);
            spuSyncQuery.setName(name);
            spuSyncQuery.setImage_list(imageList);
            spuSyncQuery.setMp_settle_type(mpSettleType);
            spuSyncQuery.setSpu_type(spuType);
            spuSyncQuery.setStatus(status);
            spuSyncQuery.setSort_weight(sortWeight);
            spuSyncQuery.setSupplier_ext_id_list(supplierExtIdList);
            spuSyncQuery.setRecommend_word(recommendWord);
            spuSyncQuery.setOrder_depends_on_date(orderDependsOnDate);
            spuSyncQuery.setFront_category_tag(frontCategoryTag);
            spuSyncQuery.setAttribute(attribute);
            spuSyncQuery.setTenantId(tenantId);
            spuSyncQuery.setClientKey(clientKey);
            return spuSyncQuery;
        }

}


    public SpuEntryInfo getEntry_info() {
        return entry_info;
    }

    public void setEntry_info(SpuEntryInfo entry_info) {
        this.entry_info = entry_info;
    }

    public List<String> getFront_category_tag() {
        return front_category_tag;
    }

    public void setFront_category_tag(List<String> front_category_tag) {
        this.front_category_tag = front_category_tag;
    }

    public String getRecommend_word() {
        return recommend_word;
    }

    public void setRecommend_word(String recommend_word) {
        this.recommend_word = recommend_word;
    }

    public Long getSort_weight() {
        return sort_weight;
    }

    public void setSort_weight(Long sort_weight) {
        this.sort_weight = sort_weight;
    }

    public List<String> getSupplier_ext_id_list() {
        return supplier_ext_id_list;
    }

    public void setSupplier_ext_id_list(List<String> supplier_ext_id_list) {
        this.supplier_ext_id_list = supplier_ext_id_list;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getSpu_ext_id() {
        return spu_ext_id;
    }

    public void setSpu_ext_id(String spu_ext_id) {
        this.spu_ext_id = spu_ext_id;
    }

    public List<String> getImage_list() {
        return image_list;
    }

    public void setImage_list(List<String> image_list) {
        this.image_list = image_list;
    }

    public Integer getMp_settle_type() {
        return mp_settle_type;
    }

    public void setMp_settle_type(Integer mp_settle_type) {
        this.mp_settle_type = mp_settle_type;
    }

    public Long getOrigin_price() {
        return origin_price;
    }

    public void setOrigin_price(Long origin_price) {
        this.origin_price = origin_price;
    }

    public Long getTake_rate() {
        return take_rate;
    }

    public void setTake_rate(Long take_rate) {
        this.take_rate = take_rate;
    }

    public JSONObject getAttribute() {
        return attribute;
    }

    public void setAttribute(JSONObject attribute) {
        this.attribute = attribute;
    }

    public List<String> getHighlights() {
        return highlights;
    }

    public void setHighlights(List<String> highlights) {
        this.highlights = highlights;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOrder_depends_on_date() {
        return order_depends_on_date;
    }

    public void setOrder_depends_on_date(Boolean order_depends_on_date) {
        this.order_depends_on_date = order_depends_on_date;
    }

    public Integer getSpu_type() {
        return spu_type;
    }

    public void setSpu_type(Integer spu_type) {
        this.spu_type = spu_type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
