package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-04-28 15:35
 **/
public class SupplierMatchResult {
    /**
     * 匹配状态描述
     */
    private String mismatch_status_desc;
    /**
     * 	POI所在省份
     */
    private String province;
    /**
     * POI地址
     */
    private String address;
    /**
     * 匹配状态，0-等待匹配，1-正在匹配，2-匹配成功，3-匹配失败
     */
    private String match_status;
    /**
     * POI名称
     */
    private String poi_name;
    /**
     *抖音POI ID
     */
    private String poi_id;
    /**
     * 第三方商户ID
     */
    private String supplier_ext_id;
    /**
     * POI所在城市
     */
    private String city;
    /**
     * 其他信息
     */
    private String extra;

    public String getMismatch_status_desc() {
        return mismatch_status_desc;
    }

    public void setMismatch_status_desc(String mismatch_status_desc) {
        this.mismatch_status_desc = mismatch_status_desc;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMatch_status() {
        return match_status;
    }

    public void setMatch_status(String match_status) {
        this.match_status = match_status;
    }

    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getSupplier_ext_id() {
        return supplier_ext_id;
    }

    public void setSupplier_ext_id(String supplier_ext_id) {
        this.supplier_ext_id = supplier_ext_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
