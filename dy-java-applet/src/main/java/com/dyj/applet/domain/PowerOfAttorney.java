package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 13:37
 **/
public class PowerOfAttorney {

    private String ext_id;

    private String url;

    public static PowerOfAttorneyBuilder builder() {
        return new PowerOfAttorneyBuilder();
    }

    public static class PowerOfAttorneyBuilder {
        private String extId;

        private String url;

        public PowerOfAttorneyBuilder extId(String extId) {
            this.extId = extId;
            return this;
        }
        public PowerOfAttorneyBuilder url(String url) {
            this.url = url;
            return this;
        }
        public PowerOfAttorney build() {
            PowerOfAttorney powerOfAttorney = new PowerOfAttorney();
            powerOfAttorney.setExt_id(extId);
            powerOfAttorney.setUrl(url);
            return powerOfAttorney;
        }
}

    public String getExt_id() {
        return ext_id;
    }

    public void setExt_id(String ext_id) {
        this.ext_id = ext_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
