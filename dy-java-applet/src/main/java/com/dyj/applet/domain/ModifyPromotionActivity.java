package com.dyj.applet.domain;

/**
 * 修改营销活动请求值
 */
public class ModifyPromotionActivity {


    /**
     * <p>需要修改的生效中的营销活动ID</p>
     */
    private String activity_id;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">描述对应营销活动的名称。</span></span></p><ul><li style="line-height: 1.38;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">为了提高信息检索效率，该名称需要保证唯一。在修改过程中平台会校验名称的唯一性</span></span></li></ul> 选填
     */
    private String activity_name;
    /**
     * <p>需要修改send_scene=4的营销活动内容时，填写结构内的字段</p> 选填
     */
    private ModifyPromotionActivityImActivity im_activity;
    /**
     * <p>需要修改send_scene=1的营销活动内容时，填写结构内的字段</p> 选填
     */
    private ModifyPromotionActivityLiveActivity live_activity;
    /**
     * <p>需要修改send_scene=5的营销活动内容时，填写结构内的字段</p> 选填
     */
    private ModifyPromotionActivitySidebarActivity sidebar_activity;

    public String getActivity_id() {
        return activity_id;
    }

    public ModifyPromotionActivity setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public ModifyPromotionActivity setActivity_name(String activity_name) {
        this.activity_name = activity_name;
        return this;
    }

    public ModifyPromotionActivityImActivity getIm_activity() {
        return im_activity;
    }

    public ModifyPromotionActivity setIm_activity(ModifyPromotionActivityImActivity im_activity) {
        this.im_activity = im_activity;
        return this;
    }

    public ModifyPromotionActivityLiveActivity getLive_activity() {
        return live_activity;
    }

    public ModifyPromotionActivity setLive_activity(ModifyPromotionActivityLiveActivity live_activity) {
        this.live_activity = live_activity;
        return this;
    }

    public ModifyPromotionActivitySidebarActivity getSidebar_activity() {
        return sidebar_activity;
    }

    public ModifyPromotionActivity setSidebar_activity(ModifyPromotionActivitySidebarActivity sidebar_activity) {
        this.sidebar_activity = sidebar_activity;
        return this;
    }

    public static ModifyPromotionActivityBuilder builder(){
        return new ModifyPromotionActivityBuilder();
    }

    public static final class ModifyPromotionActivityBuilder {
        private String activity_id;
        private String activity_name;
        private ModifyPromotionActivityImActivity im_activity;
        private ModifyPromotionActivityLiveActivity live_activity;
        private ModifyPromotionActivitySidebarActivity sidebar_activity;

        private ModifyPromotionActivityBuilder() {
        }

        public ModifyPromotionActivityBuilder activityId(String activityId) {
            this.activity_id = activityId;
            return this;
        }

        public ModifyPromotionActivityBuilder activityName(String activityName) {
            this.activity_name = activityName;
            return this;
        }

        public ModifyPromotionActivityBuilder imActivity(ModifyPromotionActivityImActivity imActivity) {
            this.im_activity = imActivity;
            return this;
        }

        public ModifyPromotionActivityBuilder liveActivity(ModifyPromotionActivityLiveActivity liveActivity) {
            this.live_activity = liveActivity;
            return this;
        }

        public ModifyPromotionActivityBuilder sidebarActivity(ModifyPromotionActivitySidebarActivity sidebarActivity) {
            this.sidebar_activity = sidebarActivity;
            return this;
        }

        public ModifyPromotionActivity build() {
            ModifyPromotionActivity modifyPromotionActivity = new ModifyPromotionActivity();
            modifyPromotionActivity.setActivity_id(activity_id);
            modifyPromotionActivity.setActivity_name(activity_name);
            modifyPromotionActivity.setIm_activity(im_activity);
            modifyPromotionActivity.setLive_activity(live_activity);
            modifyPromotionActivity.setSidebar_activity(sidebar_activity);
            return modifyPromotionActivity;
        }
    }
}
