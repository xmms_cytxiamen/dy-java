package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 查询用户可用券信息请求值
 * @author ws
 **/
public class QueryCouponReceiveInfoQuery extends UserInfoQuery {

    /**
     *
     */
    private String app_id;
    /**
     *  选填
     */
    private String coupon_id;
    /**
     * 券实例状态，不传表示查全部 选填
     */
    private Integer coupon_status;

    public String getApp_id() {
        return app_id;
    }

    public QueryCouponReceiveInfoQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public QueryCouponReceiveInfoQuery setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
        return this;
    }

    public Integer getCoupon_status() {
        return coupon_status;
    }

    public QueryCouponReceiveInfoQuery setCoupon_status(Integer coupon_status) {
        this.coupon_status = coupon_status;
        return this;
    }

    public static QueryCouponReceiveInfoQueryBuilder builder(){
        return new QueryCouponReceiveInfoQueryBuilder();
    }


    public static final class QueryCouponReceiveInfoQueryBuilder {
        private String app_id;
        private String coupon_id;
        private Integer coupon_status;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private QueryCouponReceiveInfoQueryBuilder() {
        }

        public static QueryCouponReceiveInfoQueryBuilder aQueryCouponReceiveInfoQuery() {
            return new QueryCouponReceiveInfoQueryBuilder();
        }

        public QueryCouponReceiveInfoQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public QueryCouponReceiveInfoQueryBuilder couponId(String couponId) {
            this.coupon_id = couponId;
            return this;
        }

        public QueryCouponReceiveInfoQueryBuilder couponStatus(Integer couponStatus) {
            this.coupon_status = couponStatus;
            return this;
        }

        public QueryCouponReceiveInfoQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public QueryCouponReceiveInfoQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryCouponReceiveInfoQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryCouponReceiveInfoQuery build() {
            QueryCouponReceiveInfoQuery queryCouponReceiveInfoQuery = new QueryCouponReceiveInfoQuery();
            queryCouponReceiveInfoQuery.setApp_id(app_id);
            queryCouponReceiveInfoQuery.setCoupon_id(coupon_id);
            queryCouponReceiveInfoQuery.setCoupon_status(coupon_status);
            queryCouponReceiveInfoQuery.setOpen_id(open_id);
            queryCouponReceiveInfoQuery.setTenantId(tenantId);
            queryCouponReceiveInfoQuery.setClientKey(clientKey);
            return queryCouponReceiveInfoQuery;
        }
    }
}
