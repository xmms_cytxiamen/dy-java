package com.dyj.applet.domain;

/**
 * 分享裂变配置
 */
public class QueryPromotionActivityImActivityShareFissionConfig {

    /**
     * <p>奖励券的模板ID</p> 选填
     */
    private Long award_coupon_id;
    /**
     * 奖励券的名称 选填
     */
    private String award_coupon_name;
    /**
     * <p>奖励的券数量</p> 选填
     */
    private Integer award_coupon_num;
    /**
     * 奖励类型
     */
    private Integer award_type;

    public Long getAward_coupon_id() {
        return award_coupon_id;
    }

    public QueryPromotionActivityImActivityShareFissionConfig setAward_coupon_id(Long award_coupon_id) {
        this.award_coupon_id = award_coupon_id;
        return this;
    }

    public String getAward_coupon_name() {
        return award_coupon_name;
    }

    public QueryPromotionActivityImActivityShareFissionConfig setAward_coupon_name(String award_coupon_name) {
        this.award_coupon_name = award_coupon_name;
        return this;
    }

    public Integer getAward_coupon_num() {
        return award_coupon_num;
    }

    public QueryPromotionActivityImActivityShareFissionConfig setAward_coupon_num(Integer award_coupon_num) {
        this.award_coupon_num = award_coupon_num;
        return this;
    }

    public Integer getAward_type() {
        return award_type;
    }

    public QueryPromotionActivityImActivityShareFissionConfig setAward_type(Integer award_type) {
        this.award_type = award_type;
        return this;
    }

    public static QueryPromotionActivityImActivityShareFissionConfigBuilder builder(){
        return new QueryPromotionActivityImActivityShareFissionConfigBuilder();
    }


    public static final class QueryPromotionActivityImActivityShareFissionConfigBuilder {
        private Long award_coupon_id;
        private String award_coupon_name;
        private Integer award_coupon_num;
        private Integer award_type;

        private QueryPromotionActivityImActivityShareFissionConfigBuilder() {
        }


        public QueryPromotionActivityImActivityShareFissionConfigBuilder awardCouponId(Long awardCouponId) {
            this.award_coupon_id = awardCouponId;
            return this;
        }

        public QueryPromotionActivityImActivityShareFissionConfigBuilder awardCouponName(String awardCouponName) {
            this.award_coupon_name = awardCouponName;
            return this;
        }

        public QueryPromotionActivityImActivityShareFissionConfigBuilder awardCouponNum(Integer awardCouponNum) {
            this.award_coupon_num = awardCouponNum;
            return this;
        }

        public QueryPromotionActivityImActivityShareFissionConfigBuilder awardType(Integer awardType) {
            this.award_type = awardType;
            return this;
        }

        public QueryPromotionActivityImActivityShareFissionConfig build() {
            QueryPromotionActivityImActivityShareFissionConfig queryPromotionActivityImActivityShareFissionConfig = new QueryPromotionActivityImActivityShareFissionConfig();
            queryPromotionActivityImActivityShareFissionConfig.setAward_coupon_id(award_coupon_id);
            queryPromotionActivityImActivityShareFissionConfig.setAward_coupon_name(award_coupon_name);
            queryPromotionActivityImActivityShareFissionConfig.setAward_coupon_num(award_coupon_num);
            queryPromotionActivityImActivityShareFissionConfig.setAward_type(award_type);
            return queryPromotionActivityImActivityShareFissionConfig;
        }
    }
}
