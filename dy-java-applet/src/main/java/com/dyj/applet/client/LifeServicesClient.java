package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.backend.ContentType;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DyProductResult;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.vo.BaseVo;
import com.dyj.applet.domain.vo.SaveGoodsProductVo;
import com.dyj.common.interceptor.ClientQueryTokenInterceptor;
import com.dyj.common.interceptor.ClientTokenInterceptor;

import java.util.List;

/**
 * 生活服务
 *
 * @author danmo
 * @date 2024-04-28 11:16
 **/
@BaseRequest(baseURL = "${domain}", contentType = ContentType.APPLICATION_JSON)
public interface LifeServicesClient {

    /**
     * 商铺同步
     *
     * @param query 入参
     * @return DyResult<SupplierSyncVo>
     */
    @Post(url = "${supplierSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierSyncVo> supplierSync(@JSONBody SupplierSyncQuery query);

    /**
     * 查询店铺
     *
     * @param query         应用信息
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierVo>
     */
    @Get(url = "${querySupplier}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierVo> querySupplier(@Var("query") BaseQuery query, @Query("supplier_ext_id") String supplierExtId);

    /**
     * 获取抖音POI ID
     *
     * @param query  应用信息
     * @param amapId 高德POI ID
     * @return DyResult<PoiIdVo>
     */
    @Get(url = "${queryPoiId}", interceptor = ClientTokenInterceptor.class)
    DyResult<PoiIdVo> queryPoiId(@Var("query") BaseQuery query, @Query("amap_id") String amapId);

    /**
     * 店铺匹配任务结果查询
     *
     * @param query           应用信息
     * @param supplierTaskIds 店铺任务ID
     * @return DyResult<SupplierTaskResultVo>
     */
    @Get(url = "${querySupplierTaskResult}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskResultVo> querySupplierTaskResult(@Var("query") BaseQuery query, @Query("supplier_task_ids") String supplierTaskIds);

    /**
     * 店铺匹配任务状态查询
     *
     * @param query         应用信息
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierTaskStatusVo>
     */
    @Get(url = "${querySupplierMatchStatus}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskStatusVo> querySupplierMatchStatus(@Var("query") BaseQuery query, @Query("supplier_ext_id") String supplierExtId);

    /**
     * 提交门店匹配任务
     *
     * @param query 入参
     * @return DyResult<SupplierSubmitTaskVo>
     */
    @Post(url = "${submitSupplierMatchTask}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierSubmitTaskVo> submitSupplierMatchTask(@JSONBody SupplierSubmitTaskQuery query);

    /**
     * 查询全部店铺信息接口(天级别请求5次)
     *
     * @param query 应用信息
     * @return DyResult<SupplierTaskVo>
     */
    @Get(url = "${queryAllSupplier}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskVo> queryAllSupplier(@Var("query") BaseQuery query);

    /**
     * 查询店铺全部信息任务返回内容
     *
     * @param query  应用信息
     * @param taskId 任务ID
     * @return DyResult<SupplierTaskVo>
     */
    @Get(url = "${querySupplierCallback}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskVo> querySupplierCallback(@Var("query") BaseQuery query, @Query("task_id") String taskId);

    /**
     * （老版本）SKU同步
     *
     * @param query 入参
     * @return DyResult<BaseVo>
     */
    @Post(url = "${skuSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<BaseVo> skuSync(@JSONBody SkuSyncQuery query);

    /**
     * （老版本）sku拉取(该接口由接入方实现)
     *
     * @param query     用户信息
     * @param spuExtId  接入方SPU ID 列表
     * @param startDate 拉取价格时间区间[start_date, end_date)
     * @param endDate   拉取价格时间区间[start_date, end_date)
     * @return DyResult<SkuHotelPullVo>
     */
    @Get(url = "${skuHotelPull}", interceptor = ClientTokenInterceptor.class)
    DyResult<SkuHotelPullVo> skuHotelPull(@Var("query") BaseQuery query, @Query("spu_ext_id") List<String> spuExtId, @Query("start_date") String startDate, @Query("end_date") String endDate);

    /**
     * （老版本）多门店SPU同步
     *
     * @param query 入参
     * @return DyResult<SpuSyncVo>
     */
    @Post(url = "${spuSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuSyncVo> spuSync(@JSONBody SpuSyncQuery query);

    /**
     * （老版本）多门店SPU状态同步
     *
     * @param query        应用信息
     * @param spuExtIdList 接入方商品ID列表
     * @param status       SPU状态， 1 - 在线; 2 - 下线
     * @return DyResult<SpuStatusVo>
     */
    @Post(url = "${spuStatusSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuStatusVo> spuStatusSync(@Var("query") BaseQuery query, @JSONBody("spu_ext_id_list") List<String> spuExtIdList, @JSONBody("status") Integer status);

    /**
     * （老版本）多门店SPU库存同步
     *
     * @param query 入参
     * @return DyResult<SpuStockVo>
     */
    @Post(url = "${spuStockSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuStockVo> spuStockSync(@JSONBody SpuStockQuery query);

    /**
     * （老版本）多门店SPU信息查询
     *
     * @param query
     * @param spuExtId                   第三方SPU ID
     * @param needSpuDraft               是否需要商品草稿数据(带有商品的审核状态，只展示最近30天的数据，并且最多最近提交的20次内)
     * @param spuDraftCount              需要商品草稿数据的数目(0-20)，超过这个范围默认返回20个
     * @param supplierIdsForFilterReason 供应商id列表，需要商品在某供应商下的过滤状态
     * @return DyResult<SpuVo>
     */
    @Get(url = "${spuQuery}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuVo> spuQuery(@Var("query") BaseQuery query, @Query("spu_ext_id") String spuExtId, @Query("need_spu_draft") Boolean needSpuDraft, @Query("spu_draft_count") Integer spuDraftCount, @Query("supplier_ids_for_filter_reason") List<String> supplierIdsForFilterReason);


    /**
     * 创建/修改团购商品
     * @param query 入参
     * @return   DyProductResult<SaveGoodsProductVo>
     */
    @Post(url = "${saveGoodsProduct}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<SaveGoodsProductVo> saveGoodsProduct(@JSONBody SaveGoodsProductQuery query);

    /**
     * 免审修改商品
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${freeAuditGoodsProduct}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> freeAuditGoodsProduct(@JSONBody FreeAuditGoodsProductQuery query);

    /**
     * 上下架商品
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${operateGoodsProduct}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> operateGoodsProduct(@JSONBody OperateGoodsProductQuery query);

    /**
     * 同步库存
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${syncGoodsStock}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> syncGoodsStock(@JSONBody SyncGoodsStockQuery query);

    /**
     * 查询商品模板
     * @param query 应用信息
     * @param categoryId 行业类目；详细见； 商品类目表
     * @param productType 商品类型 1 : 团购套餐 3 : 预售券 4 : 日历房 5 : 门票 7 : 旅行跟拍 8 : 一日游 11 : 代金券
     * @return DyProductResult<GoodsTemplateVo>
     */
    @Get(url = "${getGoodsTemplate}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsTemplateVo> getGoodsTemplate(@Var("query") BaseQuery query, @Query("category_id") String categoryId, @Query("product_type") Integer productType);

    /**
     * 查询商品草稿数据
     * @param query 应用信息
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds 外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductDraftVo>
     */
    @Get(url = "${getGoodsProductDraft}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductDraftVo> getGoodsProductDraft(@Var("query") BaseQuery query, @Query("product_ids") String productIds,@Query("out_ids") String outIds);

    /**
     * 查询商品线上数据
     * @param query 应用信息
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds 外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductOnlineVo>
     */
    @Get(url = "${getGoodsProductOnline}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductOnlineVo> getGoodsProductOnline(@Var("query") BaseQuery query, @Query("product_ids") String productIds,@Query("out_ids") String outIds);

    /**
     * 查询商品线上数据列表
     * @param query 应用信息
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count 分页数量，不传默认为5
     * @param status 过滤在线状态 1-在线 2-下线 3-封禁
     * @return DyProductResult
     */
    @Get(url = "${queryGoodsProductOnlineList}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductOnlineVo> queryGoodsProductOnlineList(@Var("query") BaseQuery query, @Query("cursor") String cursor, @Query("count") Integer count, @Query("status") Integer status);

    /**
     * 查询商品草稿数据列表
     * @param query 应用信息
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count 分页数量，不传默认为5
     * @param status 过滤草稿状态，10-审核中 12-审核失败 1-审核通过
     * @return DyProductResult<GoodsProductDraftVo>
     */
    @Get(url = "${queryGoodsProductDraftList}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductDraftVo> queryGoodsProductDraftList(@Var("query") BaseQuery query, @Query("cursor") String cursor, @Query("count") Integer count, @Query("status") Integer status);

    /**
     * 创建/更新多SKU商品的SKU列表
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${batchSaveGoodsSku}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> batchSaveGoodsSku(@JSONBody BatchSaveGoodsSkuQuery query);

    /**
     * 查询商品品类
     * @param query 应用信息
     * @param categoryId 行业类目ID，返回当前id下的直系子类目信息；传0或者不传，均返回所有一级行业类目
     * @param accountId 服务商的入驻商户ID/代运营的商户ID，不传时默认为服务商身份
     * @return GoodsCategoryVo
     */
    @Get(url = "${getGoodsCategory}", interceptor = ClientQueryTokenInterceptor.class)
    GoodsCategoryVo getGoodsCategory(@Var("query") BaseQuery query, @Query("category_id") String categoryId, @Query("account_id") String accountId);
}
