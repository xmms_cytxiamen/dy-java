package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dtflys.forest.backend.ContentType;
import com.dyj.applet.domain.CreatePromotionActivityV2;
import com.dyj.applet.domain.ModifyPromotionActivity;
import com.dyj.applet.domain.QueryPromotionActivityV2;
import com.dyj.applet.domain.TalentCouponLimit;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.ConsumeCouponIdListVo;
import com.dyj.applet.domain.vo.CreatePromotionActivityVo;
import com.dyj.applet.domain.vo.QueryCouponReceiveInfoVo;
import com.dyj.applet.domain.vo.QueryPromotionActivityV2Vo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 小程序券
 */
@BaseRequest(baseURL = "${domain}",contentType = ContentType.APPLICATION_JSON)
public interface PromotionCouponClient {

    /**
     * 查询用户可用券信息
     * @param body 查询用户可用券信息请求值
     * @return
     */
    @Post(value = "${queryCouponReceiveInfo}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryCouponReceiveInfoVo> queryCouponReceiveInfo(@JSONBody QueryCouponReceiveInfoQuery body);


    /**
     * 用户撤销核销券
     * @param body 用户撤销核销券请求值
     * @return
     */
    @Post(value = "${batchRollbackConsumeCoupon}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<ConsumeCouponIdListVo> batchRollbackConsumeCoupon(@JSONBody BatchRollbackConsumeCouponQuery body);

    /**
     * 复访营销活动实时圈选用户
     * @param body 复访营销活动实时圈选用户请求值
     * @return
     */
    @Post(value = "${bindUserToSidebarActivity}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> bindUserToSidebarActivity(@JSONBody BindUserToSidebarActivityQuery body);

    /**
     * 用户核销券
     * @param body 用户核销券请求值
     * @return
     */
    @Post(value = "${batchConsumeCoupon}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<ConsumeCouponIdListVo> batchConsumeCoupon(@JSONBody BatchConsumeCouponQuery body);

    /**
     * 查询主播发券配置信息
     * @param body 查询主播发券配置信息请求值
     * @return
     */
    @Post(value = "${queryTalentCouponLimit}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<TalentCouponLimit> queryTalentCouponLimit(@JSONBody QueryTalentCouponLimitQuery body);

    /**
     * 修改主播发券权限状态
     * @param body 修改主播发券权限状态请求值
     * @return
     */
    @Post(value = "${updateTalentCouponStatus}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> updateTalentCouponStatus(@JSONBody UpdateTalentCouponStatusQuery body);

    /**
     * 更新主播发券库存上限
     * @param body 更新主播发券库存上限请求值
     * @return
     */
    @Post(value = "${updateTalentCouponStock}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> updateTalentCouponStock(@JSONBody UpdateTalentCouponStockQuery body);

    /**
     * 主播发券权限配置
     * @param body 主播发券权限配置请求值
     * @return
     */
    @Post(value = "${setTalentCouponApi}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> setTalentCouponApi(@JSONBody SetTalentCouponApiQuery body);

    /**
     * 创建营销活动
     * @param body 创建营销活动请求值
     * @return
     */
    @Post(value = "${createPromotionActivityV2}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreatePromotionActivityVo> createPromotionActivityV2(@JSONBody PromotionActivityQuery<CreatePromotionActivityV2> body);

    /**
     * 修改营销活动
     * @param body 修改营销活动请求值
     * @return
     */
    @Post(value = "${modifyPromotionActivityV2}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> modifyPromotionActivityV2(@JSONBody PromotionActivityQuery<ModifyPromotionActivity> body);

    /**
     * 查询营销活动
     * @param body 查询营销活动请求值
     * @return
     */
    @Post(value = "${queryPromotionActivityV2}", interceptor = ClientTokenInterceptor.class)
    QueryPromotionActivityV2Vo queryPromotionActivityV2(@JSONBody QueryPromotionActivityV2Query body);


    /**
     * 修改营销活动状态
     * @param body 修改营销活动状态请求值
     * @return
     */
    @Post(value = "${updatePromotionActivityStatusV2}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> updatePromotionActivityStatusV2(@JSONBody UpdatePromotionActivityStatusV2Query body);
}
